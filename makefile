VERSION := 0.0.0
CC := cc
WINC := i686-w64-mingw32-gcc 
CFLAGS := -g 
FILENAME := ImageEncrypt_$(VERSION)
HEADERS = src/main.h

$(FILENAME): obj/*.o 
	$(CC) $(inputs) -o $(output) -lm

obj/%.o: src/%.c $(HEADERS)
	$(CC) $(CFLAGS) -c $(input) -o $(output)

$(FILENAME).exe: obj/win/*.o 
	$(WINC) $(inputs) -o $(output) -lm

obj/win/%.o: src/%.c $(HEADERS)
	$(WINC) $(CFLAGS) -c $(input) -o $(output)
	
