#include "main.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int main(int argc, char **argv){
	header headerDat;
	char *input;
	printf("%d\n",argc);
	for(int i=0;i<argc;i++)
	printf("%s\n",argv[i]);
	if(argc==2){
		input = argv[1];
	}
	if(argc==1){
		input = malloc(1000);
		scanf("%1000[^\n]",input);
	}

	//Format input right away
	char *formatted = malloc(strlen(input)*sizeof(char));
	formatted = format(input,3);	
	
	//Create Pixel Array
	int xSize = (int)sqrt((double)strlen(input));
	int ySize = xSize;
	if(strlen(input)%(xSize*ySize)!=0) ySize++;
	char *pixelArray = pixelize(input,xSize,ySize);
	int pad_ySize = ySize+(4-ySize%4)%4;
	int pad_Len = xSize*pad_ySize;	
	//Header shtuffs. Assume file header is 14 bytes, and DIB header is 40 bytes.
	headerDat.imgWidth=ySize;
	headerDat.imgHeight=xSize;
	headerDat.bitPixel=3;
	headerDat.pixSize=pad_Len;
	headerDat.headerSize=40;//Can I make this assumption??? Maybe.
	headerDat.fileSize=14+40+headerDat.pixSize;
	headerDat.pixOffset=14+40;

	//Final printing debug check. Make sure all our numbers are right.
	printf("File Size:%x|%d\n",headerDat.fileSize,headerDat.fileSize);
	printf("Offset to Pixel Array:%x|%d\n",headerDat.pixOffset,headerDat.pixOffset);
	printf("Header Size:%x|%d\n",headerDat.headerSize,headerDat.headerSize);
	printf("Image Width:%x|%d\n",headerDat.imgWidth,headerDat.imgWidth);
	printf("Image Height:%x|%d\n",headerDat.imgHeight,headerDat.imgHeight);
	printf("Image Size:%x|%d\n",headerDat.pixSize,headerDat.pixSize);	

	//Now make an actual array containing the entire array of data	
	char finalArray[headerDat.fileSize];
	//Signaturre	
	finalArray[0] = 'B';
	finalArray[1] = 'M';	
	packArray(finalArray,2,4,headerDat.fileSize); //File size
	packArray(finalArray,6,4,0); //Reserved
	packArray(finalArray,10,4,headerDat.pixOffset);

	//DIB Header
	packArray(finalArray,14,4,headerDat.headerSize);
	packArray(finalArray,18,4,headerDat.imgWidth);
	packArray(finalArray,22,4,headerDat.imgHeight);
	packArray(finalArray,26,2,1);//Planes
	packArray(finalArray,28,2,3*8); // Because possible reasons.
	packArray(finalArray,30,4,0);//Compression
	packArray(finalArray,34,4,headerDat.pixSize);
	packArray(finalArray,38,4,2835);//X Res
	packArray(finalArray,42,4,2835);//Y Res
	packArray(finalArray,46,4,0);//Colors in da table
	packArray(finalArray,50,4,0);//# of important colors

	//PIXEL ARRAY
	for(int i=0;i<headerDat.pixSize;i++)
		finalArray[headerDat.pixOffset+i]=pixelArray[i];

	FILE *image;
	image = fopen("output.bmp","wb+");
	for(int i=0;i<headerDat.fileSize;i++)
		fputc(finalArray[i],image);

	fclose(image);
	free(formatted);
	
	return 0;
}
