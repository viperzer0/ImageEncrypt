#include <stdlib.h>
char *pixelize(char *input, int xSize, int ySize){
	char *pixelArray=malloc(sizeof(char)*xSize*(ySize+(4-ySize%4)%4)); //Adjust for padding
	for(int i=0;i<xSize;i++){
		for(int j=0;j<ySize;j++)
			pixelArray[i*ySize+j]=input[i*ySize+j];
			
		for(int j=ySize;j<ySize+(4-ySize%4)%4;j++) //Fill up padding for each row.
			pixelArray[i*ySize+j]=0;
	}
	return pixelArray;
}
