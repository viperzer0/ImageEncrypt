#include "main.h"
#include <math.h>
//Note to self: ^ is not a power operator. It's a bitwise XOR. That would make a difference.

void packArray(char *array, int startPos, int alloc, int value){
	int curVal=value;
	for(int i=alloc-1;i>=0;i--){
		int power = pow(256,i);	
		array[startPos+i]=(curVal/power);
		curVal=(curVal-(256*(curVal/power)));
	}
}
	
		
