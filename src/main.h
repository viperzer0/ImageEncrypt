#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED
typedef struct{
	int fileSize; 
	int pixOffset;
	int headerSize;
	int imgWidth;
	int imgHeight;
	int bitPixel;
	int pixSize;
	//int horizRez;
	//int vertRez;
} header;
char *format(char *input, int blockSize);
char *pixelize(char *input, int xSize, int ySize);
void packArray(char *array, int startPos, int alloc, int value);
#endif

